#!/usr/bin/env raku
#
#  Tasks - TODO list manager
#  Copyright (c) 2022 Andrij Mizyk
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
use v6.d;

constant $VERSION='12.2022';
constant $TEXTLEN=68;

if !"$*HOME/.tasks".IO.d { mkdir "$*HOME/.tasks" }

my $tasks = "$*HOME/.tasks/tasks".IO;
my $today = DateTime.now.dd-mm-yyyy('.').chop(5);  # дд.мм
my @tasks = $tasks.lines.map(*.split('|').Array);

sub main {
	if !@*ARGS || '-h'|'--help' eq @*ARGS[0] { help }
	elsif '-v'|'--version' eq @*ARGS[0] { $VERSION.Version.say }

	elsif 'add' eq @*ARGS[0] {
		if 1 == @*ARGS { die 'не вказано текст завдання' }
		elsif 2 == @*ARGS { add @*ARGS[1] }
	}
	elsif @*ARGS[0] ~~ /^'list:'(.+)/ {
		if @*ARGS > 1 { die 'list: забагато арґументів' }
		elsif 't' eq $0 { list ':t' }
		elsif 'o' eq $0 { list ':o' }
		else { die "list: невідомий фільтр `$0`" }
	}
	elsif 'list' eq @*ARGS[0] {
		if 1 == @*ARGS { list }
		elsif 2 == @*ARGS { list "@*ARGS[1]" }
	}
	elsif @*ARGS[0] ~~ /^'do'(.+)/ {
		1 == @*ARGS ?? done [$0] !! die 'do: забагато арґументів'
	}
	elsif 'do'|'del' eq @*ARGS[0] {
		1 < @*ARGS ?? done @*ARGS[1..*] !! die 'не вказано ід'
	}
	elsif 'edit' eq @*ARGS[0] {
		if 1 == @*ARGS { die 'не вказано ід' }
		elsif 2 == @*ARGS { edit @*ARGS[1] }
	}
	elsif 'shed' eq @*ARGS[0] {
		2 < @*ARGS ?? shed @*ARGS[1..*-2],@*ARGS[*-1]
		!! die 'не вказано дату'
	}
	else { die "невідома команда `@*ARGS[0]`" }
}

sub add($_) {
	die 'текст перевищує обмеження' if $TEXTLEN < $_.chars;
	save @tasks.push: [$_,$today];
	prod '+',1
}

sub list($opt is copy = ':all') {
	return put '(нема завдань)' if !$tasks.f;

	sub printout(@_) {
		my $idw = @_>>.key.max.chars;
		my $titlew = @_.map(*.value[0].chars).max;
		for @_ {
			my ($title,$date) = $_.value;
			printf "%*d  %-*s  %s\n",
				$idw,$_.key,$titlew,$title,$date;
		}
	}
	sub f_all { printout @tasks.pairs }
	sub f_today { printout @tasks.pairs.grep({$_.value[1] ~~ /$today/}) }
	sub f_ovrd {
		my @dates = @tasks>>[1].grep({
			   $_.split('.')[1] == $today.split('.')[1]
			&& $_.split('.')[0] < $today.split('.')[0]
			|| $_.split('.')[1] < $today.split('.')[1]
		});
		if !@dates { put '(список порожній)'; return }
		printout @tasks.pairs.grep({$_.value[1] (elem) @dates})
	}
	sub f_kywd($word) {
		printout @tasks.pairs.grep({$_.value[0] ~~ /$word/})
	}

	   if ':all' eq $opt { f_all }
	elsif ':t'|':today' eq $opt { f_today }
	elsif ':o'|':overdue' eq $opt { f_ovrd }
	else { f_kywd $opt }

	printf "\n(+%d . -%d)\n",prod('',0);

	put DateTime.now(formatter=>{
		sprintf('%s, %02d.%02d.%04d %02d:%02d',
			<пн вт ср чт пт сб нд>[.day-of-week -1],
			.day,.month,.year,.hour,.minute)
	})
}

sub done(@_) {
	@tasks.splice($_,1) for @_>>.Int.sort.reverse;
	@tasks ?? save @tasks !! unlink $tasks;
	put "ЗАКРИТО {+@_} ({@_})";
	prod '-',+@_
}

sub edit($_) {
	my $text = prompt '> ';
	die 'текст перевищує обмеження' if $TEXTLEN < $text.chars;
	@tasks[$_][0] = $text;
	save @tasks
}

sub shed(@ids,$date is copy) {
	if 'today' eq $date {
		$date = $today
	}
	elsif 'tomorrow' eq $date {
		$date = DateTime.now.later(day=>+1).dd-mm-yyyy('.').chop(5)
	}
	elsif $date ~~ /^'+'(\d)$/ {
		$date = DateTime.now.later(day=>+$0).dd-mm-yyyy('.').chop(5)
	}
	elsif $date !~~ /\d**2\.\d**2/ {
		die 'незрозуміла дата'
	}
	for (^@tasks).grep(* == any @ids) { @tasks[$_][1] = $date }
	save @tasks;
	put "ПЕРЕНЕСЕНО ({@ids}) на $date"
}

sub save(@_) {
	spurt $tasks,@_.sort(*[1])>>.join('|').join("\n")~"\n"
}

sub prod($act,$count) {
	my $prod_f = "$*HOME/.tasks/prod".IO;
	my ($date,$add,$rem) = $prod_f.slurp.split(",");
	$add = $rem = 0 if $date ne $today;
	$add += $count if $act eq '+';
	$rem += $count if $act eq '-';
	spurt $prod_f,"$today,$add,$rem";
	($add,$rem)
}

sub help {
	print q:to/END/;
	Викор.: task <команда> [арґументи]

	Команди:
	add "текст"
	list [слово|фільтри]
	do ід..
	edit ід
	shed ід.. (дд.мм|today|tomorrow|+n)

	Фільтри:
	:all
	:t,:today
	:o,:overdue

	Загальні параметри:
	-v,--version  Номер версії
	-h,--help     Довідка
	END
}

main
